Most popular CPU and RAM in Mini Pcs
====================================

See more info in the [README](https://github.com/linuxhw/DMI).

Contents
--------

1. [ Devices in Mini Pcs ](#devices)
   * [ Cpu ](#cpu)
   * [ Memory ](#memory)

Devices
-------

Count  — number of computers with this device installed,
Probe  — latest probe ID of this device.

### Cpu

| MFG        | Name                           | GHz  | Count | Probe      |
|------------|--------------------------------|------|-------|------------|
| Intel      | Celeron N3050                  | 1.60 | 6     | BD1F4F70F7 |
| Intel      | Core i3-5010U                  | 2.10 | 5     | 078CF691FD |
| Intel      | Pentium N3700                  | 1.60 | 5     | EDB53070CD |
| Intel      | Core i5-3210M                  | 2.50 | 4     | D6FE9DA999 |
| Intel      | Core i3-7100U                  | 2.40 | 3     | 26108721CF |
| Intel      | Core i5-6260U                  | 1.80 | 3     | 97355011D7 |
| Intel      | Core i7-6770HQ                 | 2.60 | 3     | E2F5056B0B |
| Intel      | Core 2 Duo P8700               | 2.53 | 2     | 12BFB27212 |
| Intel      | Core i5-2520M                  | 2.50 | 2     | 44CEFD50DE |
| Intel      | Core i7-3615QM                 | 2.30 | 2     | BD87D7D9AA |
| Intel      | Core i5-4260U                  | 1.40 | 2     | 0DD740DC8F |
| AMD        | E-350                          |      | 1     | 24D55A791E |
| AMD        | E2-1800 APU with Radeon HD ... |      | 1     | 8D13B8BFE3 |
| Intel      | Celeron J4005                  | 2.00 | 1     | 24BE20A939 |
| Intel      | Pentium Silver J5005           | 1.50 | 1     | A9B6F206AB |
| Intel      | Genuine T1200                  | 1.50 | 1     | E78329BE05 |
| Intel      | Core i3-8109U                  | 3.00 | 1     | AD22664770 |
| Intel      | Core i5-8259U                  | 2.30 | 1     | F1A9B27E5C |
| Intel      | Core i7-8650U                  | 1.90 | 1     | F1331B62E6 |
| Intel      | Core i5-7260U                  | 2.20 | 1     | A48C087C97 |
| Intel      | Core i7-7567U                  | 3.50 | 1     | 070C06EF96 |
| Intel      | Core i7-8809G                  | 3.10 | 1     | 1BAD6D6321 |
| Intel      | Pentium G4560T                 | 2.90 | 1     | 9D7BDB2727 |
| Intel      | Xeon E3-1245 v6                | 3.70 | 1     | 1ECDA8C9DB |
| Intel      | Core 2 Duo P7550               | 2.26 | 1     | 2C45E4CE2A |
| Intel      | Core 2 Duo P8800               | 2.66 | 1     | C2DA8438C2 |
| Intel      | Atom D410                      | 1.66 | 1     | FD127FD8BF |
| Intel      | Atom D525                      | 1.80 | 1     | 4727851853 |
| Intel      | Core i5-2415M                  | 2.30 | 1     | 32E92CBA5F |
| Intel      | Core i7-2635QM                 | 2.00 | 1     | 4D008C616B |
| Intel      | Atom D2550                     | 1.86 | 1     | 345175C61D |
| Intel      | Atom D2700                     | 2.13 | 1     | 760206E0C9 |
| Intel      | Atom Z3735F                    | 1.33 | 1     | F339ADE022 |
| Intel      | Celeron 1007U                  | 1.50 | 1     | 3486F30CEE |
| Intel      | Core i7-3770T                  | 2.50 | 1     | F8F1E1681B |
| Intel      | Core i7-4770T                  | 2.50 | 1     | 4E3E10AE15 |
| Intel      | Core i5-5250U                  | 1.60 | 1     | ED17922D0B |
| Intel      | Core i5-5300U                  | 2.30 | 1     | 4AB80B03C5 |
| Intel      | Core i7-5557U                  | 3.10 | 1     | 44CAA27AAD |
| Intel      | Core i5-4200U                  | 1.60 | 1     | B590007CCF |
| Intel      | Core i5-4278U                  | 2.60 | 1     | B59D405F5E |
| Intel      | Celeron N3150                  | 1.60 | 1     | 6157F936DB |
| Intel      | Core i5-6400T                  | 2.20 | 1     | C263278BF1 |
| Intel      | Core i7-6700T                  | 2.80 | 1     | 305342DF08 |

### Memory

| MFG        | Name               | Size     | Type | MT/s | Count | Probe      |
|------------|--------------------|----------|------|------|-------|------------|
| Kingston   | 99U5428-018.A00... | 8192 MB  | DDR3 | 1600 | 4     | 44CAA27AAD |
| Kingston   | KHX1600C9S3L/8G... | 8192 MB  | DDR3 | 1600 | 3     | 305342DF08 |
| Crucial    | CT51264BF160B.C... | 4096 MB  | DDR3 | 1600 | 2     | 250AC69560 |
| Elpida     | Module SODIMM      | 2048 MB  | DDR3 | 1600 | 2     | 2B957DDA7E |
| Kingston   | 9905594-001.A00... | 2048 MB  | DDR3 | 1600 | 2     | 2DD6AE3565 |
| Micron     | 8KTF51264HZ-1G9... | 4096 MB  | DDR3 | 1600 | 2     | A317F5B87E |
| Samsung    | Module SODIMM      | 2048 MB  | DDR3 | 1067 | 2     | 12BFB27212 |
| Samsung    | Module SODIMM      | 2048 MB  | DDR3 | 1333 | 2     | BD87D7D9AA |
| Samsung    | Module DIMM        | 4096 MB  | DDR3 | 1333 | 2     | 44CEFD50DE |
|            | Module SODIMM      | 4096 MB  | DDR3 | 1333 | 1     | 4D008C616B |
| A-DATA     | AM1L16BC2P1-B2B... | 2048 MB  | DDR3 | 1600 | 1     | 5DF12FB380 |
| Apacer     | Module DIMM        | 2048 MB  | DDR2 | 800  | 1     | FD127FD8BF |
| Corsair    | CMSX32GX4M2A240... | 16384 MB | DDR4 | 2400 | 1     | F1331B62E6 |
| Corsair    | Module SODIMM      | 4096 MB  | DDR3 | 1333 | 1     | 32E92CBA5F |
| Crucial    | CT16G4SFD8213.C... | 16384 MB | DDR4 | 2133 | 1     | C263278BF1 |
| Crucial    | CT4G4SFS8213.C8... | 4096 MB  | DDR4 | 2133 | 1     | 78F26D3B40 |
| Crucial    | CT4G4SFS824A.C8... | 4096 MB  | DDR4 | 2400 | 1     | AD22664770 |
| Crucial    | CT4G4SFS824A.M8... | 4096 MB  | DDR4 | 2400 | 1     | 24BE20A939 |
| Crucial    | CT51264BF160BJ.... | 4096 MB  | DDR3 | 1600 | 1     | 24D55A791E |
| Crucial    | CT8G4SFS8213.M8... | 8192 MB  | DDR4 | 2133 | 1     | 7C4C7CAF9A |
| Crucial    | Module SODIMM      | 8192 MB  | DDR3 | 1600 | 1     | 55112C2DEF |
| G.Skill    | F4-2133C15-8GRS... | 8192 MB  | DDR4 | 2133 | 1     | A48C087C97 |
| G.Skill    | F4-2400C16-16GR... | 16384 MB | DDR4 | 2400 | 1     | 1BAD6D6321 |
| G.Skill    | F4-2400C16-4GRS... | 4096 MB  | DDR4 | 2400 | 1     | A9B6F206AB |
| SK Hynix   | HMT325S6CFR8C-H... | 2048 MB  | DDR3 | 1333 | 1     | 8D13B8BFE3 |
| SK Hynix   | Module SODIMM      | 16384 MB | DDR4 | 2400 | 1     | 1ECDA8C9DB |
| Kingston   | 9905417-074.A00... | 4096 MB  | DDR3 | 1333 | 1     | E8C6542553 |
| Kingston   | 9905469-136.A00... | 4096 MB  | DDR3 | 1600 | 1     | BD1F4F70F7 |
| Kingston   | 9905594-001.A00... | 2048 MB  | DDR3 | 1600 | 1     | 1403645254 |
| Kingston   | 9905594-014.A00... | 2048 MB  | DDR3 | 1600 | 1     | 523FFD7861 |
| Kingston   | 9905663-005.A00... | 16384 MB | DDR4 | 2400 | 1     | 97355011D7 |
| Kingston   | 99U5469-011.A01... | 2048 MB  | DDR3 | 1066 | 1     | 760206E0C9 |
| Kingston   | 99U5469-045.A00... | 4096 MB  | DDR3 | 1066 | 1     | 345175C61D |
| Kingston   | 99U5469-045.A00... | 4096 MB  | DDR3 | 1600 | 1     | 3486F30CEE |
| Kingston   | 99U5469-051.A00... | 4096 MB  | DDR3 | 1600 | 1     | D8C76E861B |
| Kingston   | HP24D4S7S8MBP-8... | 8192 MB  | DDR4 | 2400 | 1     | 32F7AA8227 |
| Kingston   | KHX1600C9S3L/4G... | 4096 MB  | DDR3 | 1600 | 1     | EDB53070CD |
| Kingston   | KHX2133C13S4/16... | 16384 MB | DDR4 | 2133 | 1     | 070C06EF96 |
| Kingston   | KHX2133C13S4/8G... | 8192 MB  | DDR4 | 2133 | 1     | 7A40FE9A5C |
| Kingston   | KHX2400C14S4/16... | 16384 MB | DDR4 | 2400 | 1     | F1A9B27E5C |
| Kingston   | KHX2400C14S4/8G... | 8192 MB  | DDR4 | 2400 | 1     | 9834E348F9 |
| Kingston   | Module SODIMM      | 8192 MB  | DDR3 | 1600 | 1     | 6C458ECDE9 |
| Kingston   | Module SODIMM      | 8192 MB  | DDR4 | 2400 | 1     | 9D7BDB2727 |
| Micron     | Module SODIMM      | 1024 MB  | DDR3 | 1067 | 1     | C2DA8438C2 |
| Micron     | Module SODIMM      | 4096 MB  | DDR3 | 1333 | 1     | D6FE9DA999 |
|            | Module SODIMM      | 2048 MB  | DDR3 | 1333 | 1     | F339ADE022 |
|            | Module             | 2048 MB  | DDR3 | 800  | 1     | 4727851853 |
|            | Module SODIMM      | 8192 MB  | DDR3 | 1600 | 1     | 6157F936DB |
| Samsung    | M471B5173DB0-YK... | 4096 MB  | DDR3 | 1600 | 1     | B590007CCF |
| Samsung    | Module SODIMM      | 2048 MB  | DDR3 | 1600 | 1     | 0DD740DC8F |
| SK Hynix   | HMA81GS6AFR8N-U... | 8192 MB  | DDR4 | 2400 | 1     | 26108721CF |
| SK Hynix   | Module SODIMM      | 1024 MB  | DDR3 | 1067 | 1     | 2C45E4CE2A |
| SK Hynix   | Module SODIMM      | 2048 MB  | DDR3 | 1600 | 1     | 029708EAF4 |
| SK Hynix   | Module SODIMM      | 4096 MB  | DDR3 | 1333 | 1     | D6FE9DA999 |
| SK Hynix   | Module SODIMM      | 4096 MB  | DDR3 | 1600 | 1     | B59D405F5E |
| SK Hynix   | Module DIMM        | 512 MB   | DDR2 | 667  | 1     | E78329BE05 |

