Most popular CPU, RAM and battery in Notebooks
==============================================

See more info in the [README](https://github.com/linuxhw/DMI).

Contents
--------

1. [ Devices in Notebooks ](#devices)
   * [ Cpu ](#cpu)
   * [ Memory ](#memory)
   * [ Battery ](#battery)

Devices
-------

Count  — number of computers with this device installed,
Probe  — latest probe ID of this device.

### Cpu

| MFG        | Name                           | GHz  | Count | Probe      |
|------------|--------------------------------|------|-------|------------|
| Intel      | Core i5-3210M                  | 2.50 | 198   | 632B661A7E |
| Intel      | Core i5-3230M                  | 2.60 | 142   | E67DFC255A |
| Intel      | Core i5-2450M                  | 2.50 | 132   | E2413CEA5D |
| Intel      | Pentium B960                   | 2.20 | 128   | 891002E8F2 |
| Intel      | Atom N450                      | 1.66 | 126   | 5D585B11E9 |
| Intel      | Core i5-2410M                  | 2.30 | 126   | 7F96DEDA8A |
| Intel      | Core i3-2350M                  | 2.30 | 124   | 6B25B8982D |
| Intel      | Celeron N2840                  | 2.16 | 124   | A1D2E02773 |
| Intel      | Atom N270                      | 1.60 | 120   | E6CF3D7F11 |
| Intel      | Core i3-3110M                  | 2.40 | 119   | B9A4817612 |
| AMD        | E-450 APU with Radeon HD Gr... |      | 114   | 8CCF84D124 |
| Intel      | Core i3-2310M                  | 2.10 | 110   | 8579BA1B16 |
| Intel      | Core i3 M 380                  | 2.53 | 105   | E6323014B6 |
| Intel      | Core i3 M 370                  | 2.40 | 103   | 8FAE94D181 |
| Intel      | Core i5-2430M                  | 2.40 | 101   | 1B6DCB6A34 |
| Intel      | Atom N2600                     | 1.60 | 98    | ADF87DAC7F |
| Intel      | Core i5-4210U                  | 1.70 | 98    | 2A224752BA |
| Intel      | Pentium 2020M                  | 2.40 | 86    | 908F38EB2A |
| Intel      | Atom N455                      | 1.66 | 83    | 7438532ADB |
| Intel      | Atom N570                      | 1.66 | 77    | 0B64C1379E |
| Intel      | Core i3-2330M                  | 2.20 | 77    | 05F2682C5B |
| Intel      | Pentium Dual-Core T4500        | 2.30 | 76    | 0CEC12C495 |
| Intel      | Core i7-2670QM                 | 2.20 | 74    | 005450877A |
| Intel      | Core i7-3630QM                 | 2.40 | 70    | 268D53A8B4 |
| Intel      | Core i5-7200U                  | 2.50 | 68    | F227611E1F |
| Intel      | Core i3-3217U                  | 1.80 | 68    | 1CA46CE89B |
| AMD        | A10-4600M APU with Radeon H... |      | 67    | 45864F9C6B |
| Intel      | Core i3-3120M                  | 2.50 | 67    | 9A401175B3 |
| Intel      | Core i5-3337U                  | 1.80 | 65    | 8123FE2145 |
| Intel      | Pentium Dual-Core T4300        | 2.10 | 64    | 73E5B46F66 |
| Intel      | Pentium P6200                  | 2.13 | 64    | 52035C5F01 |
| Intel      | Pentium M processor            | 1.20 | 63    | D5D7DAB02F |
| Intel      | Core i5-2520M                  | 2.50 | 63    | 28C0349E06 |
| Intel      | Core i5-4200U                  | 1.60 | 63    | 4E682AC524 |
| Intel      | Core i5 M 460                  | 2.53 | 61    | 84F06D63E2 |
| Intel      | Core i5-6200U                  | 2.30 | 61    | BEF9865EC7 |
| Intel      | Pentium N3540                  | 2.16 | 59    | 7297A28A7F |
| Intel      | Core i7-2630QM                 | 2.00 | 58    | 1F578B22B4 |
| Intel      | Core i3-5005U                  | 2.00 | 57    | 9E9396445A |
| Intel      | Core i7-7700HQ                 | 2.80 | 56    | CB9F8EC46D |
| Intel      | Core i5-5200U                  | 2.20 | 56    | 456A3DFFE5 |
| Intel      | Pentium Dual-Core T4400        | 2.20 | 55    | 36F0A1FAAD |
| Intel      | Pentium Dual-Core T4200        | 2.00 | 53    | 571AB72073 |
| Intel      | Celeron N2830                  | 2.16 | 53    | 903B641CA7 |
| Intel      | Core i7-3610QM                 | 2.30 | 53    | 3EFE65FA87 |
| Intel      | Core i7-6700HQ                 | 2.60 | 53    | 7E4A6B2354 |
| Intel      | Core 2 Duo P8400               | 2.26 | 52    | 37B037257F |
| Intel      | Celeron N3050                  | 1.60 | 52    | 5762619036 |
| Intel      | Core i5-8250U                  | 1.60 | 51    | 376C2CCA98 |
| Intel      | Core i3 M 330                  | 2.13 | 51    | 3EBC919E62 |
| Intel      | Core i3 M 350                  | 2.27 | 51    | 838DE3CB18 |
| Intel      | Core i3-6006U                  | 2.00 | 51    | 3E47232411 |
| Intel      | Core i5-3317U                  | 1.70 | 50    | E3825A8890 |
| Intel      | Core i3-4005U                  | 1.70 | 50    | 966882EE7D |
| Intel      | Core i5 M 480                  | 2.67 | 49    | 4C990A61B6 |
| Intel      | Pentium P6100                  | 2.00 | 49    | 86987CF1ED |
| Intel      | Core i5-4200M                  | 2.50 | 49    | DEDB8E43CB |
| Intel      | Celeron N3060                  | 1.60 | 49    | 11C32DDA05 |
| Intel      | Core 2 Duo T5750               | 2.00 | 47    | 71B1D42943 |
| Intel      | Pentium B950                   | 2.10 | 46    | 54ADC646D6 |
| Intel      | Celeron 2955U                  | 1.40 | 46    | C7C9017A3C |
| AMD        | E-300 APU with Radeon HD Gr... |      | 45    | F9F3745636 |
| Intel      | Core i7-8550U                  | 1.80 | 45    | 5F7A70586E |
| Intel      | Core i7-8750H                  | 2.20 | 45    | 9CA695A346 |
| Intel      | Atom Z3735F                    | 1.33 | 45    | ED81FBB6E1 |
| Intel      | Celeron 1005M                  | 1.90 | 45    | 298DAC5F6C |
| Intel      | Core i3-2370M                  | 2.40 | 44    | 69DE877AA2 |
| AMD        | C-60 APU with Radeon HD Gra... |      | 43    | 599BB74F20 |
| AMD        | A8-4500M APU with Radeon HD... |      | 43    | 92AE8C0F3B |
| Intel      | Core i7-4700MQ                 | 2.40 | 43    | 08F8DE28C7 |
| Intel      | Celeron B800                   | 1.50 | 42    | 241AE2B69C |
| Intel      | Pentium 2117U                  | 1.80 | 42    | EFA97B65E3 |
| Intel      | Celeron 900                    | 2.20 | 41    | A4A296B8BB |
| Intel      | Core 2 Duo T6600               | 2.20 | 41    | 167B3FD913 |
| Intel      | Core i5 M 430                  | 2.27 | 41    | 6C36A45E07 |
| Intel      | Celeron B820                   | 1.70 | 41    | F28D18F3E9 |
| Intel      | Pentium B940                   | 2.00 | 41    | 877057DB38 |
| AMD        | E-350                          |      | 40    | 5E3F89149F |
| Intel      | Celeron 540                    | 1.86 | 40    | 52B80877DA |
| Intel      | Core 2 Duo T7500               | 2.20 | 38    | E1C83EA9AE |
| Intel      | Core i7-4500U                  | 1.80 | 38    | FA252F5949 |
| AMD        | A6-6310 APU with AMD Radeon... |      | 37    | 00C22F3924 |
| Intel      | Core i3-4000M                  | 2.40 | 37    | 9EF7CE41C7 |
| Intel      | Core 2 Duo T5550               | 1.83 | 36    | CC01255AAA |
| Intel      | Core i5-3320M                  | 2.60 | 36    | 0D769EAB0A |
| Intel      | Core i3-4030U                  | 1.90 | 36    | 1DEA266689 |
| Intel      | Core 2 T7200                   | 2.00 | 35    | 19A4B7F9E2 |
| Intel      | Atom N2800                     | 1.86 | 35    | F923747F5E |
| Intel      | Core i3-4010U                  | 1.70 | 35    | FDD61F096B |
| AMD        | A4-3300M APU with Radeon HD... |      | 34    | 937DD869FC |
| AMD        | C-50                           |      | 34    | 377FEBC80C |
| Intel      | Celeron M processor            | 1.50 | 33    | 48522ED650 |
| Intel      | Pentium N3710                  | 1.60 | 33    | 6458C4F07B |
| Intel      | Core i7-6500U                  | 2.50 | 33    | A586F57112 |
| AMD        | Athlon II P320 Dual-Core       |      | 32    | 1A24B89466 |
| AMD        | E1-1200 APU with Radeon HD ... |      | 32    | A312BDDCD7 |
| AMD        | A6-4400M APU with Radeon HD... |      | 32    | 01D1980A2A |
| AMD        | A4-5000 APU with Radeon HD ... |      | 32    | 12801C9BDF |
| Intel      | Core 2 Duo T5800               | 2.00 | 32    | 77E3B20E26 |
| Intel      | Celeron B815                   | 1.60 | 32    | FE2AB22987 |
| Intel      | Pentium B970                   | 2.30 | 32    | 2D672E25E0 |
| Intel      | Core i7-4710HQ                 | 2.50 | 32    | 3A17D38BDD |
| AMD        | Athlon II P340 Dual-Core       |      | 31    | 33969DDCE8 |
| Intel      | Celeron Dual-Core T3100        | 1.90 | 31    | A7EC10F5EE |
| Intel      | Core 2 Duo T8100               | 2.10 | 31    | E5DE762918 |
| Intel      | Core 2 Duo T5250               | 1.50 | 30    | FAECBEFA9B |
| Intel      | Pentium Dual T2390             | 1.86 | 30    | BD8A19714A |
| Intel      | Core 2 Duo P8700               | 2.53 | 30    | 530502BEDD |
| Intel      | Pentium B980                   | 2.40 | 30    | 19CDBF743C |
| Intel      | Celeron 1000M                  | 1.80 | 30    | 2D8991FAAF |
| Intel      | Core 2 Duo T7250               | 2.00 | 29    | E3B1F6D810 |
| Intel      | Core 2 T5500                   | 1.66 | 29    | 719BE91D02 |
| Intel      | Core i5 M 520                  | 2.40 | 29    | 2AFD83B0D3 |
| Intel      | Core i7-4510U                  | 2.00 | 29    | FD24FFF375 |
| Intel      | Core i3-6100U                  | 2.30 | 29    | 047BD9BF6C |
| AMD        | E2-1800 APU with Radeon HD ... |      | 28    | 9E99F18B81 |
| Intel      | Celeron Dual-Core T3000        | 1.80 | 28    | 370DCD58CC |
| Intel      | Celeron B830                   | 1.80 | 28    | 923B4CD756 |
| Intel      | Core i7-3632QM                 | 2.20 | 28    | 5E69C5F864 |
| Intel      | Core i7-4700HQ                 | 2.40 | 28    | 2A3F7B30CC |
| Intel      | Pentium N3700                  | 1.60 | 28    | 9964879FBE |
| AMD        | A6-3400M APU with Radeon HD... |      | 27    | 2C903F36D0 |
| AMD        | A10-5750M APU with Radeon H... |      | 27    | FBC2F0A547 |
| AMD        | E1-2100 APU with Radeon HD ... |      | 27    | 7C8E8CFB76 |
| Intel      | Core i7-7500U                  | 2.70 | 27    | 67672EF038 |
| Intel      | Celeron D 220                  | 1.20 | 27    | 9B7F075594 |
| Intel      | Atom N550                      | 1.50 | 27    | 75D4E1070C |
| AMD        | E1-6010 APU with AMD Radeon... |      | 26    | 8B0DFDBE84 |
| Intel      | Core 2 Duo T5450               | 1.66 | 26    | 7B7903FC02 |
| Intel      | Pentium Dual T2370             | 1.73 | 26    | 4629286F1F |
| Intel      | Core 2 Duo P8600               | 2.40 | 26    | 80ECBDE76E |
| Intel      | Core i7 Q 720                  | 1.60 | 26    | CE5A8DEAB0 |
| Intel      | Core 2 Duo T7300               | 2.00 | 25    | 46963F5E35 |
| Intel      | Pentium Dual T3200             | 2.00 | 25    | 643DD308BF |
| Intel      | Celeron Dual-Core T3500        | 2.10 | 25    | 79612D82B7 |
| Intel      | Core i5 M 560                  | 2.67 | 25    | D588CD38C2 |
| Intel      | Core i7-3537U                  | 2.00 | 25    | D8DB450D73 |
| Intel      | Pentium Dual T2330             | 1.60 | 24    | 4E9E1B53EE |
| Intel      | Celeron 530                    | 1.73 | 24    | F027D59B91 |
| Intel      | Celeron 560                    | 2.13 | 24    | F8EBFCA78D |
| Intel      | Core 2 Duo T8300               | 2.40 | 24    | 184B996E7B |
| AMD        | A6-3420M APU with Radeon HD... |      | 23    | 9A249DA8EE |
| AMD        | A8-3500M APU with Radeon HD... |      | 23    | 4BBB490EBC |
| AMD        | A8-7410 APU with AMD Radeon... |      | 23    | CC12720E5E |
| Intel      | Core i5-2540M                  | 2.60 | 23    | C90A6CB2C9 |
| Intel      | Pentium N3530                  | 2.16 | 23    | C6BC466B79 |
| AMD        | A6-7310 APU with AMD Radeon... |      | 22    | 1CC2218397 |
| Intel      | Core 2 T5600                   | 1.83 | 22    | 92ACB586D2 |
| Intel      | Core 2 Duo P7350               | 2.00 | 22    | 508F60B315 |
| Intel      | Pentium P6000                  | 1.87 | 22    | 60DCE95FE1 |
| Intel      | Core i7-5500U                  | 2.40 | 22    | 6D5DE9B567 |
| AMD        | Phenom II N830 Triple-Core     |      | 21    | 690FFF1815 |
| AMD        | A8-6410 APU with AMD Radeon... |      | 21    | 01958BC5B8 |
| Intel      | Core 2 Duo T7100               | 1.80 | 21    | CC8859DB0B |
| Intel      | Celeron 1007U                  | 1.50 | 21    | 9ED9651BBD |
| Intel      | Core i5-4210M                  | 2.60 | 21    | C149167D1D |
| AMD        | A6-5200 APU with Radeon HD ... |      | 20    | E0D133BEFC |
| Intel      | Pentium Dual T3400             | 2.16 | 20    | 2855B8B6EC |
| Intel      | Core i3-2328M                  | 2.20 | 20    | A08CF5DD76 |
| Intel      | Atom x5-Z8350                  | 1.44 | 20    | DBBBD6582E |
| Intel      | Pentium N4200                  | 1.10 | 20    | 0C521EF4A6 |
| Intel      | Celeron M 430                  | 1.73 | 19    | BDD475BFCC |
| Intel      | Core 2 Duo T9300               | 2.50 | 19    | 7D62551961 |
| Intel      | Core i3 M 350                  | 2.27 | 19    | 3BF80303F6 |
| Intel      | Core i7-3520M                  | 2.90 | 19    | 7C3288572E |
| Intel      | Core i7-4702MQ                 | 2.20 | 19    | CA32F6C410 |
| Intel      | Celeron 550                    | 2.00 | 18    | 77770402E0 |
| Intel      | Core 2 Duo P8600               | 2.40 | 18    | A3C4D07088 |
| Intel      | Atom N435                      | 1.33 | 18    | ADF740C910 |
| Intel      | Celeron M 440                  | 1.86 | 17    | 2841935AF9 |
| Intel      | Genuine T2300                  | 1.66 | 17    | 42BA942A6D |
| Intel      | Atom Z520                      | 1.33 | 17    | 70E60207C2 |
| Intel      | Pentium 987                    | 1.50 | 17    | 2227196AFC |
| Intel      | Core i7-3517U                  | 1.90 | 17    | 3935C68D79 |
| Intel      | Atom x5-Z8300                  | 1.44 | 17    | E080900E16 |
| AMD        | Athlon 64 X2 Dual-Core TK-55   |      | 16    | CA1FB717A1 |
| AMD        | A4-3305M APU with Radeon HD... |      | 16    | B9085E4B0A |
| Intel      | Core 2 Duo T6400               | 2.00 | 16    | AC65EDA96F |
| Intel      | Core i5 M 520                  | 2.40 | 16    | 9E7380CFB4 |
| Intel      | Core i5 M 450                  | 2.40 | 16    | D023F50697 |
| Intel      | Core i5-4200H                  | 2.80 | 16    | F810A8999A |
| Intel      | Core i7-4710MQ                 | 2.50 | 16    | E2B52AF1A5 |
| Intel      | Core i7-4720HQ                 | 2.60 | 16    | 1EFF42042A |
| Intel      | Celeron N3350                  | 1.10 | 16    | 176970ABB8 |
| AMD        | Turion 64 X2 Mobile Technol... |      | 15    | D3BABC6991 |
| AMD        | Turion II Dual-Core Mobile ... |      | 15    | 11191760C2 |
| Intel      | Pentium M processor            | 1.80 | 15    | 1811B66E00 |
| Intel      | Genuine T2250                  | 1.73 | 15    | 11C4A1EA1A |
| Intel      | Core 2 Duo T6500               | 2.10 | 15    | AA3D8D6F76 |
| Intel      | Core i3-2367M                  | 1.40 | 15    | 397D29D2F2 |
| Intel      | Atom N2100                     | 1.60 | 15    | 359A01778D |
| Intel      | Core i7-3612QM                 | 2.10 | 15    | 744AF8E456 |
| AMD        | Phenom II N930 Quad-Core       |      | 14    | ACFD67A4EA |
| AMD        | A9-9420 RADEON R5, 5 COMPUT... |      | 14    | 024CF11765 |
| AMD        | E1-2500 APU with Radeon HD ... |      | 14    | 6479F7F12B |
| Intel      | Core 2 Duo T5670               | 1.80 | 14    | C15AB53D21 |
| Intel      | Core 2 T5300                   | 1.73 | 14    | 95AEEBC902 |
| Intel      | Core i5-7300HQ                 | 2.50 | 14    | 04B0989528 |
| Intel      | Core 2 Duo T9400               | 2.53 | 14    | 73D90C4661 |
| Intel      | Core i3-3227U                  | 1.90 | 14    | DE168F8DCC |
| Intel      | Core i5-5300U                  | 2.30 | 14    | 73DB320B5F |
| AMD        | Turion 64 Mobile Technology... |      | 13    | 8C1579107F |
| AMD        | Phenom II P960 Quad-Core       |      | 13    | 8AB2BC95FD |
| AMD        | Athlon II Dual-Core M320       |      | 13    | A61379EEF1 |
| AMD        | Athlon II P360 Dual-Core       |      | 13    | 8C66675AD7 |
| Intel      | Genuine T2400                  | 1.83 | 13    | CF41AB5F1A |
| Intel      | Core i3-7100U                  | 2.40 | 13    | 0B84D724C8 |
| Intel      | Core i5-8300H                  | 2.30 | 13    | 4CF82380D8 |
| Intel      | Core 2 Duo T6570               | 2.10 | 13    | 9E4109323B |
| Intel      | Atom D525                      | 1.80 | 13    | FAE8940A87 |
| Intel      | Pentium N3520                  | 2.16 | 13    | 1C41DC7384 |
| Intel      | Atom Z3735G                    | 1.33 | 13    | 8F9E198F6E |
| Intel      | Core i5-4210H                  | 2.90 | 13    | 21544EE35C |
| AMD        | Turion 64 X2 Mobile Technol... |      | 12    | 5CF0A693D2 |
| AMD        | Turion 64 X2 Mobile Technol... |      | 12    | CEED13B4FC |
| AMD        | A4-4300M APU with Radeon HD... |      | 12    | 462F89483C |
| Intel      | Core Duo T2450                 | 2.00 | 12    | 6047C02215 |
| Intel      | Genuine T2080                  | 1.73 | 12    | 5BBE95113B |
| Intel      | Core i7-8650U                  | 1.90 | 12    | EB49973873 |
| Intel      | Core i7-2640M                  | 2.80 | 12    | 3ACD13490F |
| AMD        | Turion 64 X2 Mobile Technol... |      | 11    | 9B49EE4D5C |
| AMD        | Turion 64 X2 Mobile Technol... |      | 11    | C6996EB488 |
| AMD        | Turion II P540 Dual-Core       |      | 11    | 8A6BBDF84F |
| AMD        | V140                           |      | 11    | EE1CA3F65D |
| AMD        | A6-9220 RADEON R4, 5 COMPUT... |      | 11    | 5F12E9FED8 |
| AMD        | A10-5745M APU with Radeon H... |      | 11    | DF94F00345 |
| Intel      | Core Duo T2250                 | 1.73 | 11    | E13C297997 |
| Intel      | Celeron M 420                  | 1.60 | 11    | A6E8CD9DE0 |
| Intel      | Celeron M 520                  | 1.60 | 11    | 16BE592F4F |
| Intel      | Core i7 Q 740                  | 1.73 | 11    | A9FD7E47B1 |
| Intel      | Core i7-2620M                  | 2.70 | 11    | 4398E73607 |
| Intel      | Celeron 1017U                  | 1.60 | 11    | 583EC484B5 |
| Intel      | Core i5-4300U                  | 1.90 | 11    | C50DF1C2F3 |
| AMD        | Athlon Neo MV-40               |      | 10    | 01B549D421 |
| AMD        | Phenom II N970 Quad-Core       |      | 10    | 7AFC4FAAC2 |
| AMD        | Turion II Dual-Core Mobile ... |      | 10    | 527313B5FF |
| AMD        | Athlon X2 Dual-Core QL-65      |      | 10    | 31A49D09FD |
| AMD        | A8-3510MX APU with Radeon H... |      | 10    | DC1A1C4F22 |
| AMD        | A4-1200 APU with Radeon HD ... |      | 10    | A7F7276E3D |
| Intel      | Pentium Silver N5000           | 1.10 | 10    | 5A0A25C9FF |
| Intel      | Core 2 Duo T7300               | 2.00 | 10    | 2B9B5142AF |
| Intel      | Core 2 Duo T5470               | 1.60 | 10    | EF87CDEC72 |
| Intel      | Core 2 Duo T5870               | 2.00 | 10    | 18D94651FD |
| Intel      | Pentium Dual T2310             | 1.46 | 10    | 54C92BF69C |
| Intel      | Core 2 T5600                   | 1.83 | 10    | 5B4BAFA432 |
| AMD        | Turion 64 X2 Mobile Technol... |      | 9     | 109CDB17E1 |
| AMD        | Turion II P520 Dual-Core       |      | 9     | 66D8646E49 |
| AMD        | Athlon X2 Dual-Core QL-60      |      | 9     | 33B91F3A3E |
| AMD        | Turion X2 Dual-Core Mobile ... |      | 9     | 42E7957235 |
| AMD        | Turion X2 Dual-Core Mobile ... |      | 9     | ECAFF22C3E |

### Memory

| MFG        | Name               | Size     | Type | MT/s | Count | Probe      |
|------------|--------------------|----------|------|------|-------|------------|
| Samsung    | M471B5273DH0-CH... | 4096 MB  | DDR3 | 1334 | 274   | E2413CEA5D |
| Samsung    | M471B5773CHS-CH... | 2048 MB  | DDR3 | 1334 | 193   | AB17752168 |
| Samsung    | M471B5173DB0-YK... | 4096 MB  | DDR3 | 1600 | 179   | FDD61F096B |
| Samsung    | M471B5273CH0-CH... | 4096 MB  | DDR3 | 1334 | 177   | 6F9734843A |
|            | Module DIMM        | 2048 MB  | DDR2 | 667  | 171   | 38190AD2E1 |
| Samsung    | M471B5773DH0-CH... | 2048 MB  | DDR3 | 1600 | 170   | 1B6DCB6A34 |
|            | Module             | 2048 MB  | DDR2 |      | 166   | A3C4D07088 |
| Samsung    | M471B5273DH0-CK... | 4096 MB  | DDR3 | 1600 | 163   | BBA96B0372 |
|            | Module             | 1024 MB  | DDR2 |      | 152   | CA1FB717A1 |
| Samsung    | M471B5173QH0-YK... | 4096 MB  | DDR3 | 1600 | 141   | 166081CE08 |
|            | SODIMM             | 2048 MB  | DDR2 | 667  | 141   | A7EC10F5EE |
|            | Module SODIMM      | 1024 MB  | DDR2 | 667  | 132   | C15AB53D21 |
| Elpida     | EBJ41UF8BCS0-DJ... | 4096 MB  | DDR3 | 1334 | 112   | 9E99F18B81 |
| Samsung    | M471B5173EB0-YK... | 4096 MB  | DDR3 | 1600 | 111   | 1CC2218397 |
|            | SODIMM             | 1024 MB  | DDR2 | 667  | 105   | 86E2030E27 |
|            | Module             | 4096 MB  | DDR3 |      | 98    | 1CA46CE89B |
| Samsung    | M471B5673FH0-CH... | 2048 MB  | DDR3 | 1334 | 98    | 973B44CBCE |
| SK Hynix   | HMT351S6CFR8C-P... | 4096 MB  | DDR3 | 1600 | 90    | 3F35BDC85A |
| 48spaces   | 012345678901234... | 1024 MB  | DDR2 | 800  | 89    | 5D585B11E9 |
| Nanya      | NT2GC64B88B0NS-... | 2048 MB  | DDR3 | 1334 | 86    | D023F50697 |
| Samsung    | M471B5773DH0-CK... | 2048 MB  | DDR3 | 1600 | 84    | 3F35BDC85A |
|            | Module DRAM        | 1024 MB  |      |      | 81    | BDD475BFCC |
|            | SODIMM             | 2048 MB  | DDR2 | 800  | 81    | 6D19C3FD33 |
| Samsung    | M471B5673FH0-CF... | 2048 MB  |      | 1067 | 80    | 8A6BBDF84F |
|            | Module DDR         | 1024 MB  |      |      | 78    | 54C92BF69C |
| Elpida     | EBJ40UG8BBU0-GN... | 4096 MB  | DDR3 | 1600 | 76    | 92AE8C0F3B |
|            | Module SODIMM      | 2048 MB  | DDR3 | 1333 | 76    | ED81FBB6E1 |
| Elpida     | EBJ21UE8BFU0-DJ... | 2048 MB  | DDR3 | 1334 | 74    | 8579BA1B16 |
| Nanya      | NT4GC64B8HB0NS-... | 4096 MB  | DDR3 | 1334 | 71    | 8CD4AF2E88 |
|            | Module SDRAM       | 2048 MB  |      |      | 71    | 2D4D5EFCB2 |
| SK Hynix   | HMT351S6CFR8C-H... | 4096 MB  | DDR3 | 1333 | 66    | 2A3F7B30CC |
| SK Hynix   | HMT425S6AFR6A-P... | 2048 MB  | DDR3 | 1600 | 65    | F652C9DD47 |
| Micron     | 16KTF51264HZ-1G... | 4096 MB  | DDR3 | 1600 | 65    | B9A4817612 |
|            | Module SDRAM       | 1024 MB  |      |      | 65    | 2841935AF9 |
| SK Hynix   | HMT325S6BFR8C-H... | 2048 MB  | DDR3 | 1334 | 65    | 1ABB956626 |
| SK Hynix   | HMT351S6CFR8C-H... | 4096 MB  | DDR3 | 1334 | 64    | E6323014B6 |
| Samsung    | M471B2873FHS-CH... | 1024 MB  | DDR3 | 1334 | 63    | 333F038ACE |
| Nanya      | NT4GC64B8HG0NS-... | 4096 MB  | DDR3 | 1333 | 62    | 68C4019E33 |
| Kingston   | ACR256X64D3S133... | 2048 MB  | DDR3 | 1334 | 61    | 86987CF1ED |
| ELPIDA     | EBJ21UE8BDS0-DJ... | 2048 MB  | DDR3 | 1334 | 60    | 530502BEDD |
| SK Hynix   | HMT451S6BFR8A-P... | 4096 MB  | DDR3 | 1600 | 60    | 6D5DE9B567 |
| SK Hynix   | HMT351S6CFR8C-P... | 4096 MB  | DDR3 | 1600 | 58    | A6777FAE90 |
|            | Module DIMM        | 512 MB   | DDR2 |      | 58    | 16BE592F4F |
| SK Hynix   | HMT325S6CFR8C-P... | 2048 MB  | DDR3 | 1600 | 57    | E3825A8890 |
|            | Module DIMM        | 2048 MB  | DDR2 | 800  | 56    | EE66F4DDC1 |
| Samsung    | M471B5173BH0-CK... | 4096 MB  | DDR3 | 1600 | 56    | 0F042024B4 |
| Micron     | 8JSF25664HZ-1G4... | 2048 MB  | DDR3 | 1333 | 55    | D588CD38C2 |
| Ramaxel    | RMT3170EB68F9W1... | 4096 MB  | DDR3 | 1600 | 53    | 72424367AD |
| Samsung    | M471B2873FHS-CF... | 1024 MB  | DDR3 | 1067 | 51    | C08F9590AD |
| Ramaxel    | RMT3160ED58E9W1... | 4096 MB  | DDR3 | 1600 | 50    | 144AE2DAF5 |
| Samsung    | M471B5273CH0-CK... | 4096 MB  | DDR3 | 1600 | 50    | 2AFD83B0D3 |
| A-DATA     | AD73I1C1674EV S... | 4096 MB  | DDR3 | 1334 | 48    | E12F2B72CE |
| Micron     | 8KTF51264HZ-1G6... | 4096 MB  | DDR3 | 1600 | 48    | 9964879FBE |
| SK Hynix   | HMT451S6BFR8A-P... | 4096 MB  | DDR3 | 1600 | 47    | D0D112CE5B |
| SK Hynix   | HMT451S6AFR8A-P... | 4096 MB  | DDR3 | 1600 | 47    | EF1164AACA |
|            | Module             | 2048 MB  | DDR3 |      | 47    | 1C35FEA298 |
| Ramaxel    | RMT3020EC58E9F1... | 4096 MB  | DDR3 | 4199 | 47    | 5E3F89149F |
| Micron     | 16JSF51264HZ-1G... | 4096 MB  | DDR3 | 1334 | 46    | 05F2682C5B |
|            | Module             | 2048 MB  |      | 667  | 45    | 0B64C1379E |
| Samsung    | M471A5244CB0-CR... | 4096 MB  | DDR4 | 2400 | 45    | 3E47232411 |
| Samsung    | M471B1G73QH0-YK... | 8192 MB  | DDR3 | 1600 | 45    | AC96DD45F9 |
| Nanya      | NT2GC64B88G0NS-... | 2048 MB  | DDR3 | 1600 | 44    | F56243C8E5 |
| Samsung    | M471B1G73DB0-YK... | 8192 MB  | DDR3 | 1600 | 43    | 3A17D38BDD |
| SK Hynix   | HMT325S6BFR8C-H... | 2048 MB  | DDR3 | 1333 | 42    | 6B25B8982D |
| Ramaxel    | RMT3010EC58E8F1... | 2048 MB  | DDR3 | 1600 | 42    | 52B3890B69 |
| Samsung    | M4 70T5663QZ3-C... | 2048 MB  | DDR2 | 2048 | 42    | B2E125B932 |
| Samsung    | M471B5673EH1-CF... | 2048 MB  |      | 4199 | 42    | 370DCD58CC |
| SK Hynix   | HMT351S6BFR8C-H... | 4096 MB  | DDR3 | 1333 | 41    | 6439D633F9 |
|            | Module             | 2048 MB  |      | 800  | 41    | 55A3613394 |
| SK Hynix   | HYMP125S64CP8-S... | 2048 MB  |      | 975  | 41    | 4A19D0FF08 |
| Nanya      | NT2GC64B8HC0NS-... | 2048 MB  | DDR3 | 1334 | 40    | D023F50697 |
| Samsung    | M471B5674QH0-YK... | 2048 MB  | DDR3 | 1600 | 40    | 2C903F36D0 |
| SK Hynix   | HMT451S6MFR8C-P... | 4096 MB  | DDR3 | 1600 | 40    | 87FE751543 |
| Kingston   | 99U5469-045.A00... | 4096 MB  | DDR3 | 1600 | 38    | CC7193A7B9 |
|            | Module DDR         | 2048 MB  |      |      | 38    | 92ACB586D2 |
|            | Module DRAM        | 2048 MB  |      |      | 38    | 666CC686BC |
|            | Module SODIMM      | 4096 MB  | DDR3 | 1333 | 38    | EE2443A541 |
| Samsung    | M471A1K43CB1-CR... | 8192 MB  | DDR4 | 2667 | 38    | 04B0989528 |
| SK Hynix   | HMT325S6CFR8C-H... | 2048 MB  | DDR3 | 1334 | 38    | 726FFF24C9 |
| SK Hynix   | HMT351S6CFR8C-P... | 4096 MB  | DDR3 | 1600 | 38    | 891002E8F2 |
|            | SODIMM             | 1024 MB  | DDR2 | 533  | 38    | 2BD735CBDF |
| Kingston   | ACR16D3LFS1KBG/... | 2048 MB  | DDR3 | 1600 | 37    | 0D73CB1291 |
| Kingston   | ACR16D3LS1KFG/4... | 4096 MB  | DDR3 | 1600 | 37    | 1DEA266689 |
| SK Hynix   | HMT351S6EFR8A-P... | 4096 MB  | DDR3 | 1600 | 35    | 4E682AC524 |
| Kingston   | ACR16D3LS1NGG/4... | 4096 MB  | DDR3 | 1600 | 35    | E67DFC255A |
|            | Module SODIMM      | 1024 MB  | DDR2 | 533  | 35    | FB7F51216E |
| SK Hynix   | HMA81GS6AFR8N-U... | 8192 MB  | DDR4 | 2400 | 35    | 90E9F8DFAD |
| SK Hynix   | HMT351S6BFR8C-H... | 4096 MB  | DDR3 | 1334 | 35    | DDA2EC4D0E |
| A-DATA     | AD73I1C1674EV S... | 4096 MB  | DDR3 | 1334 | 34    | 0BFEB87324 |
| Micron     | 8KTF25664HZ-1G6... | 2048 MB  | DDR3 | 1600 | 34    | 700E6ABAE1 |
| Samsung    | M471B5273EB0-CK... | 4096 MB  | DDR3 | 1600 | 34    | 7C8E8CFB76 |
| ELPIDA     | EBJ20UF8BCS0-DJ... | 2048 MB  | DDR3 | 1334 | 33    | 1476158702 |
| SK Hynix   | HMT451S6AFR8A-P... | 4096 MB  | DDR3 | 1600 | 33    | 07CD36611F |
| Samsung    | M471B1G73EB0-YK... | 8192 MB  | DDR3 | 1600 | 32    | 91BF899CC4 |
| ASint      | SSY3128M8-EDJEF... | 1024 MB  | DDR3 | 1333 | 31    | 6B25B8982D |
| ASint      | SSZ3128M8-EDJEF... | 2048 MB  | DDR3 | 1333 | 30    | DB1BD4EC31 |
| SK Hynix   | HMT351S6EFR8C-P... | 4096 MB  | DDR3 | 1600 | 30    | A82FAD253C |
| SK Hynix   | HMA851S6AFR6N-U... | 4096 MB  | DDR4 | 2667 | 30    | F3941C8871 |
| ELPIDA     | EBJ21UE8BDS0-AE... | 2048 MB  | DDR3 | 1067 | 29    | 673B5CB9B0 |
| Nanya      | NT4GC64B8HG0NS-... | 4096 MB  | DDR3 | 1600 | 29    | 4FB6C852BB |
|            | Module SDRAM       | 512 MB   |      |      | 29    | C99061F8D3 |
| Ramaxel    | RMT3170ME68F9F1... | 4096 MB  | DDR3 | 1600 | 29    | E1D3B73A71 |
| Samsung    | M4 70T5663EH3-C... | 2048 MB  | DDR2 | 2048 | 29    | 1F49A84F1F |
| Samsung    | M471B5773CHS-CF... | 2048 MB  | DDR3 | 1067 | 29    | 7AFC4FAAC2 |
| SK Hynix   | HMT451S6AFR8A-P... | 4096 MB  | DDR3 | 1600 | 29    | 4FC85D1C31 |
| SK Hynix   | HMT425S6AFR6A-P... | 2048 MB  | DDR3 | 1600 | 28    | 1DEA266689 |
|            | Module             | 1024 MB  | DDR2 | 333  | 28    | 3F91F53F96 |
|            | Module DIMM        | 2048 MB  | DDR2 | 533  | 28    | B4C2FB4D5A |
|            | Module DDR         | 512 MB   |      |      | 28    | C662BAA8F5 |
| Samsung    | M471B5273CH0-YK... | 4096 MB  | DDR3 | 1600 | 28    | B53D815277 |
| Elpida     | Module SODIMM      | 4096 MB  | DDR3 | 1600 | 27    | 9604BDACB2 |
| Kingston   | 99U5469-041.A00... | 4096 MB  | DDR3 | 1600 | 26    | 908F38EB2A |
| Micron     | 4ATF51264HZ-2G3... | 4096 MB  | DDR4 | 2400 | 26    | 5F12E9FED8 |
| Nanya      | M2S4G64CB8HG5N-... | 4096 MB  | DDR3 | 1334 | 26    | 3A6CB2F9EA |
| Elpida     | EBJ40UG8EFU0 SO... | 4096 MB  | DDR3 | 1600 | 25    | BEE231AA07 |
| SK Hynix   | HMT325S6CFR8C-H... | 2048 MB  | DDR3 | 1333 | 25    | 68C4019E33 |
| Samsung    | M4 70T2953EZ3-C... | 1024 MB  |      | 667  | 25    | A17761D3F8 |
| SK Hynix   | HMT41GS6BFR8A-P... | 8192 MB  | DDR3 | 1600 | 24    | 73DB320B5F |
| Micron     | 8KTF51264HZ-1G6... | 4096 MB  | DDR3 | 1600 | 24    | 966882EE7D |
|            | Module DIMM        | 4096 MB  | DDR3 | 1600 | 24    | 64DC3ACE03 |
| Samsung    | M471B5273CM0-CH... | 4096 MB  | DDR3 | 1333 | 24    | 6D637319C4 |
| SK Hynix   | HMT351S6EFR8A-P... | 4096 MB  | DDR3 | 1600 | 24    | 1061ACDC06 |
|            | SODIMM             | 1024 MB  | DDR2 | 800  | 24    | 6D19C3FD33 |
| Elpida     | EBJ40UG8EFU0-GN... | 4096 MB  | DDR3 | 1600 | 23    | 010241E3A2 |
| SK Hynix   | HMT325S6CFR8C-P... | 2048 MB  | DDR3 |      | 23    | A321FB83E7 |
| Kingston   | 99U5469-046.A00... | 4096 MB  | DDR3 | 1333 | 23    | E03A8C0C89 |
| Micron     | 16JSF25664HZ-1G... | 2048 MB  | DDR3 | 1067 | 23    | BBA96B0372 |
| Micron     | MT8KTF51264HZ-1... | 4096 MB  | DDR3 | 1600 | 23    | EFA97B65E3 |
|            | Module DRAM        | 512 MB   |      |      | 23    | BDD475BFCC |
| Samsung    | M471B5173BH0-YK... | 4096 MB  | DDR3 | 1600 | 23    | DEDB8E43CB |
| Samsung    | Module SODIMM      | 1024 MB  | DDR2 | 533  | 23    | 77770402E0 |
| SHARETR... | Module SODIMM      | 4096 MB  | DDR3 | 1600 | 23    | 10D1795169 |
| SK Hynix   | HMT112S6TFR8C-H... | 1024 MB  | DDR3 | 1333 | 23    | 239DB5D282 |
| SK Hynix   | HYMP512S64CP8-Y... | 1024 MB  | DDR2 | 667  | 23    | CF41AB5F1A |
|            | SODIMM             | 2048 MB  | DDR2 | 533  | 23    | F027D59B91 |
| A-DATA     | AM1L16BC2P1-B1F... | 2048 MB  | DDR3 | 1600 | 22    | 6479F7F12B |
| SK Hynix   | HMT351S6BFR8C-H... | 4096 MB  | DDR3 | 1333 | 22    | 937DD869FC |
| SK Hynix   | HMT451S6MFR8C-P... | 4096 MB  | DDR3 | 1600 | 22    | DAB56079A8 |
| Kingston   | 99U5428-040.A01... | 4096 MB  | DDR3 | 1334 | 22    | 19D5DAD934 |
| Kingston   | 99U5428-046.A00... | 4096 MB  | DDR3 | 1334 | 22    | 3EFE65FA87 |
| Kingston   | 99U5428-063.A00... | 8192 MB  | DDR3 | 1600 | 22    | B9A4817612 |
| Kingston   | ACR256X64D3S13C... | 2048 MB  | DDR3 | 1334 | 22    | 8E79CDDBDA |
| Micron     | 16KTF1G64HZ-1G6... | 8192 MB  | DDR3 | 1600 | 22    | E2EB1F026B |
|            | Module DIMM        | 2048 MB  | DDR3 | 1600 | 22    | EB632C0D63 |
| SK Hynix   | HYMP112S64CP6-Y... | 1024 MB  |      | 667  | 22    | E5DE762918 |
| Crucial    | CT51264BF160B.C... | 4096 MB  | DDR3 | 1600 | 21    | 6F9734843A |
| SK Hynix   | HMT325S6BFR8C-H... | 2048 MB  | DDR3 | 1334 | 21    | 8A1811EA2D |
| SK Hynix   | HYMP125S64CP8-S... | 2048 MB  | DDR2 | 800  | 21    | AC65EDA96F |
| Kingston   | 99U5428-018.A00... | 8192 MB  | DDR3 | 1600 | 21    | 456A3DFFE5 |
| Micron     | Module SODIMM      | 4096 MB  | DDR3 | 1600 | 21    | 9CF06D20FA |
|            | Module             | 2048 MB  | DDR2 | 400  | 21    | E69E0C49CF |
| A-DATA     | AM1U16BC4P2-B19... | 4096 MB  | DDR3 | 1600 | 20    | 268D53A8B4 |
| Kingston   | ACR256X64D3S133... | 2048 MB  | DDR3 | 1333 | 20    | DDDC5B738C |
| Nanya      | NT2GT64U8HD0BN-... | 2048 MB  | DDR2 | 800  | 20    | B95AF01C35 |
| Ramaxel    | RMT3170EF68F9W1... | 4096 MB  | DDR3 | 1600 | 20    | E40C63AC8F |
| Samsung    | M4 70T2864QZ3-C... | 1024 MB  | DDR2 | 2048 | 20    | B2E125B932 |
| Toshiba    | 64T128020EDL2.5... | 2048 MB  |      | 1066 | 20    | A4A296B8BB |
| SK Hynix   | HMT41GS6AFR8A-P... | 8192 MB  | DDR3 | 1600 | 19    | 9C035C7658 |
| SK Hynix   | Module DIMM        | 4096 MB  | DDR3 | 1600 | 19    | C370427F60 |
| Kingston   | ACR16D3LS1KNG/4... | 4096 MB  | DDR3 | 1600 | 19    | EA5E934FE8 |
| Kingston   | 414352313238583... | 1024 MB  | DDR2 | 667  | 19    | F81C51DA3D |
|            | Module             | 2048 MB  | DDR2 | 333  | 19    | CC8859DB0B |
| Samsung    | M471A1K43BB1-CR... | 8192 MB  | DDR4 | 2400 | 19    | C07258B9AB |
| SK Hynix   | HMT425S6AFR6A-P... | 2048 MB  | DDR3 | 1600 | 19    | 7BBCABAC04 |
| SK Hynix   | HYMP125S64CP8-Y... | 2048 MB  | DDR2 | 667  | 19    | 63AD37FEBB |
| A-DATA     | AM1U16BC4P2-B19... | 4096 MB  | DDR3 | 1600 | 18    | 19D5DAD934 |
| SK Hynix   | HMT351S6EFR8A-P... | 4096 MB  | DDR3 | 1600 | 18    | 709C282E30 |
| Kingston   | KHX1600C9S3L/8G... | 8192 MB  | DDR3 | 1600 | 18    | E2B52AF1A5 |
| Kingston   | KNWMX1-ETB SODIMM  | 4096 MB  | DDR3 | 1600 | 18    | C50DF1C2F3 |
| Samsung    | M471A5244CB0-CT... | 4096 MB  | DDR4 | 2667 | 18    | 4CF82380D8 |
| Samsung    | M471B5773CHS-CK... | 2048 MB  | DDR3 | 1600 | 18    | 7C82C563B9 |
| SK Hynix   | HMT325S6CFR8C-P... | 2048 MB  | DDR3 | 1600 | 18    | 234D6648BD |
| Toshiba    | 8HTF12864HDY-80... | 2048 MB  |      | 1066 | 18    | 57BAB28E56 |
| SK Hynix   | HMA851S6AFR6N-U... | 4096 MB  | DDR4 | 2400 | 17    | 1CCEC0E2DB |
| SK Hynix   | HYMP125S64CP8-S... | 2048 MB  | DDR2 | 2048 | 17    | 527313B5FF |
| Kingston   | ACR16D3LS1NGG/2... | 2048 MB  | DDR3 | 1600 | 17    | D5D973011D |
| Kingston   | ASU16D3LS1KBG/4... | 4096 MB  | DDR3 | 1600 | 17    | 024331191D |
| Kingston   | KHX1600C9S3L/4G... | 4096 MB  | DDR3 | 1600 | 17    | 7C0CD24986 |
|            | Module             | 1024 MB  |      | 667  | 17    | 95EF90B2D8 |
| Samsung    | M4 70T5663QZ3-C... | 2048 MB  | DDR2 | 667  | 17    | 571AB72073 |
| Unifosa    | GU672203EP0200 ... | 1024 MB  | DDR3 | 1333 | 17    | 2AC7FB9C4D |
| ASint      | SSZ302G08-GGNED... | 2048 MB  | DDR3 | 1600 | 16    | AC4B774153 |
| SK Hynix   | HMT451S6AFR8C-P... | 4096 MB  | DDR3 | 1600 | 16    | A4911B787A |
| Kingston   | HP16D3LS1KFG/4G... | 4096 MB  | DDR3 | 1600 | 16    | FE2AB22987 |
| SK Hynix   | HMT125S6BFR8C-G... | 2048 MB  |      | 1067 | 16    | DCDBB14F35 |
| SK Hynix   | HMT451S6BFR8A-P... | 4096 MB  | DDR3 |      | 16    | CE5A8DEAB0 |
| ASint      | SSZ302G08-GDJEC... | 2048 MB  | DDR3 | 1333 | 15    | 02D1F84F6B |
| Crucial    | CT102464BF160B.... | 8192 MB  | DDR3 | 1600 | 15    | 2DCF55334F |
| Goldkey    | BKH400SO25608-1... | 4096 MB  | DDR3 | 1334 | 15    | 3CCACD3312 |
| Goldkey    | BKH400SO51208-1... | 4096 MB  | DDR3 | 1333 | 15    | 0FDFC9C3FF |
| SK Hynix   | HMT351S6CFR8C-P... | 4096 MB  | DDR3 | 1600 | 15    | AC4B774153 |
| Kingston   | ACR16D3LS1NBG/4... | 4096 MB  | DDR3 | 1600 | 15    | EB8B49B7B8 |
|            | Module             | 1024 MB  |      | 800  | 15    | 32251B3B6D |
| Ramaxel    | RMT3020EF48E8W1... | 2048 MB  | DDR3 | 1334 | 15    | 73353ECAA7 |
| Ramaxel    | RMT3150ED58E8W1... | 2048 MB  | DDR3 | 1600 | 15    | B9CC4FFB0F |
| Ramaxel    | RMT3170MN68F9F1... | 4096 MB  | DDR3 | 1600 | 15    | 00C22F3924 |
| Samsung    | M4 70T2864QZ3-C... | 1024 MB  |      | 1639 | 15    | FDD2BA2C8B |
| Samsung    | M471A1K43BB0-CP... | 8192 MB  | DDR4 | 2133 | 15    | 425176719A |
| Samsung    | M471B5173CB0-YK... | 4096 MB  | DDR3 | 1600 | 15    | 1D59A8F384 |
| Samsung    | M471B5273CH0-CF... | 4096 MB  | DDR3 | 1067 | 15    | 408D0B84F9 |
| SK Hynix   | HMA41GS6AFR8N-T... | 8192 MB  | DDR4 | 2133 | 15    | BE887070FE |
| SK Hynix   | Module DIMM        | 1024 MB  | DDR2 | 533  | 15    | F8EBFCA78D |
| SK Hynix   | Module DIMM        | 512 MB   | DDR2 | 533  | 15    | 8169D60D35 |
|            | SODIMM SODIMM      | 2048 MB  |      | 800  | 15    | 4AA3E959E1 |
| Corsair    | CMSO4GX3M1A1333... | 4096 MB  | DDR3 | 1333 | 14    | 877057DB38 |
| Crucial    | CT102464BF160B.... | 8192 MB  | DDR3 | 1600 | 14    | 492A8BD77B |
| ELPIDA     | EBJ20UF8BDU0-GN... | 2048 MB  | DDR3 | 1600 | 14    | D9903B4749 |
| Elpida     | EDJ4216EFBG-GNL... | 4096 MB  | DDR3 | 1600 | 14    | 9ED9651BBD |
| SK Hynix   | HMT325S6CFR8C-P... | 2048 MB  | DDR3 | 1600 | 14    | 539459E889 |
| Kingston   | ACR128X64D3S133... | 1024 MB  | DDR3 | 1333 | 14    | 3A03A2C1A2 |
| Micron     | 16JSF25664HZ-1G... | 2048 MB  | DDR3 | 1334 | 14    | 62146F7126 |
| Micron     | 8ATF1G64HZ-2G3B... | 8192 MB  | DDR4 | 2400 | 14    | D09201D834 |
| Micron     | 8ATF1G64HZ-2G6E... | 8192 MB  | DDR4 | 2667 | 14    | 79E53D2DB5 |
| Nanya      | NT1GT64U8HB0BN-... | 1024 MB  |      | 667  | 14    | 53DD0DBCA2 |
| Qimonda    | 64T128021EDL3SB... | 1024 MB  |      | 667  | 14    | 09CA7997AC |
|            | Module             | 1024 MB  | DDR3 | 1333 | 14    | 8F9E198F6E |
|            | Module             | 2048 MB  | DDR3 | 800  | 14    | 1D126E3917 |
| Ramaxel    | RMT3170MK58F8F1... | 2048 MB  | DDR3 | 1600 | 14    | EBBF8CD8D4 |
| Samsung    | M471B2873GB0-CH... | 1024 MB  | DDR3 | 1333 | 14    | 6DCBA6331C |
| Samsung    | M471B5173BH0-CH... | 4096 MB  | DDR3 | 1334 | 14    | 0C37A1C3D1 |
| SK Hynix   | HYMP112S64CP6-S... | 1024 MB  | DDR2 | 800  | 14    | B6E64FB455 |
|            | SODIMM             | 512 MB   | DDR2 | 533  | 14    | A698B9428B |
| A-DATA     | AD73I1B1672EG D... | 2048 MB  |      | 4199 | 13    | 8074C57902 |
| Crucial    | CT51264BF160BJ.... | 4096 MB  | DDR3 | 1600 | 13    | 624BA17241 |
| Elpida     | EBJ10UE8BDS0-DJ... | 1024 MB  | DDR3 | 1334 | 13    | 5310D49B44 |
| SK Hynix   | HMT325S6EFR8A-P... | 2048 MB  | DDR3 | 1600 | 13    | 69CEE0DC90 |
| SK Hynix   | HMT425S6CFR6A-P... | 2048 MB  | DDR3 | 1600 | 13    | 91CFED00DB |
| Kingston   | ACR256X64D3S13C... | 2048 MB  | DDR3 | 1333 | 13    | 31AA3BF86E |
| Samsung    | M471A5244BB0-CP... | 4096 MB  | DDR4 | 2133 | 13    | ED62F572C5 |
| Samsung    | M471B5674EB0-YK... | 2048 MB  | DDR3 | 1600 | 13    | 4FA9406328 |
| SHARETR... | Module SODIMM      | 2048 MB  | DDR3 | 1600 | 13    | 298DAC5F6C |
| SK Hynix   | HMT351S6EFR8C-P... | 4096 MB  | DDR3 | 1600 | 13    | 18B471C4F8 |
| A-DATA     | AM1U16BC2P1-B1A... | 2048 MB  | DDR3 | 4199 | 12    | 735024F9ED |
|            | 123456789012345... | 2048 MB  |      | 2133 | 12    | 176970ABB8 |
| Elpida     | EBJ10UE8BDS0-AE... | 1024 MB  | DDR3 | 1067 | 12    | 3D6D998C96 |
| Goldkey    | GKH200SO25608-1... | 2048 MB  | DDR3 | 1600 | 12    | 100EB984CA |
| Goldkey    | GKH400SO51208-1... | 4096 MB  | DDR3 | 1600 | 12    | 78B210025D |
|            | Module             | 1024 MB  | DDR3 | 667  | 12    | 1249AFC67D |
|            | Module             | 2048 MB  | DDR3 | 667  | 12    | 75D4E1070C |
| Ramaxel    | RMT3170ED58F8W1... | 2048 MB  | DDR3 | 1600 | 12    | A91A253A4D |
| Samsung    | M471A1G43DB0-CP... | 8192 MB  | DDR4 | 2133 | 12    | 8BDEEB7120 |
| Samsung    | M471A1K43CB1-CT... | 8192 MB  | DDR4 | 2667 | 12    | 383A34EDD1 |
| Samsung    | M471A5143EB0-CP... | 4096 MB  | DDR4 | 2133 | 12    | 3E47232411 |
| Samsung    | M471A5244BB0-CR... | 4096 MB  | DDR4 | 2400 | 12    | D02CB45D1E |
| SK Hynix   | HMT125S6TFR8C-H... | 2048 MB  | DDR3 | 1334 | 12    | 14BF9C4413 |
| SK Hynix   | HMT451S6MFR8A-P... | 4096 MB  | DDR3 | 1600 | 12    | 86DD7BDE7C |
| A-DATA     | Module SODIMM      | 4096 MB  | DDR3 | 1333 | 11    | 5A6F9CC354 |
| Elpida     | EBJ41UF8BDU0-GN... | 4096 MB  | DDR3 | 1600 | 11    | 0802F0777E |
| Goldkey    | BKH800SO51208-1... | 8192 MB  | DDR3 | 1600 | 11    | 32648531B2 |
| Goldkey    | GKH400SO25616-1... | 4096 MB  | DDR3 | 1600 | 11    | A43D18B1F2 |

### Battery

| MFG        | Name          | Wh    | Type    | Count | Probe      |
|------------|---------------|-------|---------|-------|------------|
| Lenovo     | PABAS0241231  | 38.8  | Li-ion  | 234   | 00C22F3924 |
| Samsung    |               | 49    | Li-ion  | 224   | 3A996CF854 |
| Compal     | PABAS0241231  | 30.9  | Li-ion  | 196   | 0F042024B4 |
| SANYO      | Li_Ion_4000mA | 47.5  | Li-ion  | 150   | D023F50697 |
| ASUSTeK    | ASUS          | 50.1  | Li-ion  | 143   | 383A34EDD1 |
| Samsung    |               | 44    | Li-ion  | 117   | 5D585B11E9 |
| Notebook   | BAT           | 49    | Li-ion  | 106   | E2B52AF1A5 |
| SANYO      | AL10B31       | 48.8  | Li-ion  | 86    | 2612D462D9 |
| LG         | Li_Ion_4000mA | 47.5  | Li-ion  | 85    | 80ADE4BAB7 |
| Hewlett... | PABAS0241231  | 41.9  | Li-ion  | 82    | 0B84D724C8 |
| ASUSTeK    | X550A26       | 37.7  | Li-ion  | 72    | 6458C4F07B |
| PANASONIC  | Li_Ion_4000mA | 47.5  | Li-ion  | 72    | F28D18F3E9 |
| ASUSTek    | K52F-44       | 46.6  | Li-ion  | 71    | 52035C5F01 |
| Hewlett... | Primary       | 48    | Li-ion  | 69    | B1ED13A854 |
| Samsung    |               | 48    | Li-ion  | 68    | 86987CF1ED |
| ASUSTEK    | F82--22       | 46.2  | Li-ion  | 62    | 38190AD2E1 |
| SANYO      | PABAS0241231  | 55.9  | Li-ion  | 62    | E1D3B73A71 |
| SANYO      | GC86508SAT0   | 41.9  | Li-ion  | 60    | 5A52F8FD85 |
| SANYO      | PABAS024      | 85.4  | Li-ion  | 60    | 26DFFE047D |
| SANYO      | AS10D31       | 47.5  | Li-ion  | 58    | 690FFF1815 |
| LGC        | 45N1049       | 47.5  | Li-ion  | 55    | 65D329A2C6 |
| SANYO      | AL12A32       | 37    | Li-ion  | 54    | EB8B49B7B8 |
| Samsung    |               | 58    | Li-ion  | 52    | 6F9734843A |
| LG         | PABAS024      | 52.7  | Li-ion  | 48    | 7F96DEDA8A |
| LG         | PABAS0241231  | 53.3  | Li-ion  | 45    | 891002E8F2 |
| Sony       | VGP-BPS26     | 45.0  | Li-ion  | 43    | B56B7F6394 |
| ASUSTeK    | K55--44       | 47.9  | Li-ion  | 42    | 9A401175B3 |
| Hewlett... | Primary       | 42    | Li-ion  | 41    | 91BF899CC4 |
| Hewlett... | Primary       | 44    | Li-ion  | 41    | 8D460232A9 |
| Samsung    |               | 24    | Li-ion  | 41    | 359A01778D |
| ASUSTEK    | F3---24       | 51.2  | Li-ion  | 40    | 26F6983C58 |
| ASUSTek    | K53--52       | 57.2  | Li-ion  | 39    | 653D157E1A |
| ASUSTeK    | X550A30       | 44.6  | Li-ion  | 39    | A329AB5204 |
| Intel SR 1 | SR Real       | 22.1  | Li-ion  | 39    | AB6B2CF0E5 |
| Hewlett... | Primary       |       | Li-ion  | 38    | A17761D3F8 |
| Hewlett... | Primary       | 39    | Li-ion  | 37    | 2CE0B2FF35 |
| ASUSTEK    | F5---22       | 47.3  | Li-ion  | 36    | 54C92BF69C |
| SIMPLO     | PABAS0241231  | 41.6  | Li-ion  | 36    | F72DD0F8DC |
| Sony       | Li_Ion_4000mA | 47.5  | Li-ion  | 36    | 07988069C9 |
| Hewlett... | 5600          | 62.1  | Li-ion  | 35    | EDC722485A |
| OEM        | standard      | 15.8  | Li-ion  | 34    | 100EB984CA |
| SANYO      | 45N1043       | 38.8  | Li-ion  | 34    | 2ACB72482B |
| SANYO      | GRAPE32       | 48.8  | Li-ion  | 34    | 77E3B20E26 |
| Hewlett... | Primary       | 45    | Li-ion  | 33    | 12D1FAA353 |
| SANYO      | L09S6Y02      | 38.8  | Li-ion  | 33    | 241AE2B69C |
| ASUSTeK    | X555-50       | 37.3  | Li-ion  | 32    | FDD61F096B |
| Hewlett... | Primary       | 40    | Li-ion  | 32    | A586F57112 |
| SANYO      | 0032334134... | 55.9  | Li-ion  | 32    | 1DEA266689 |
| ASUSTek    | K53--27       | 37.8  | Li-ion  | 31    | 55E49B0471 |
| ASUSTEK    | T12--22       | 720.8 | Li-ion  | 31    | 8D9FBEF758 |
| Hewlett... | Primary       | 46    | Li-ion  | 30    | 83EDBDA556 |
| SANYO      | AL15A32       | 37    | Li-ion  | 30    | 6D1C781DE0 |
| ASUSTek    | X101CH        | 23.7  | Li-ion  | 29    | B6B9B50BF1 |
| SMP        | L09M6Y02      | 48.8  | Li-ion  | 29    | 678D443649 |
| TKBSS      | NS2P3SZMC4WR  | 48.4  | Li-ion  | 29    | 52592CBE1C |
|            | 47.52         | 48    | Li-ion  | 28    | 92F9AF7165 |
| ASUSTek    | 1015PE        | 56.9  | Li-ion  | 28    | 0B64C1379E |
| Lenovo     |               | 28    |         | 28    | C3DB42E6C3 |
| Hewlett... | Primary       | 41    | Li-ion  | 27    | 9246996D88 |
| Hewlett... | Primary       | 43    | Li-ion  | 27    | AC96DD45F9 |
| Hewlett... | Primary       | 55    | Li-ion  | 27    | AC65EDA96F |
| LGC        | AC14B8K       | 48.9  | Li-ion  | 27    | C5279AB6E4 |
| Samsung    | SR Real       | 43.0  | Li-ion  | 27    | 268D53A8B4 |
| ASUSTek    | M50--24       | 51.2  | Li-ion  | 26    | 37A9E829F4 |
| ASUSTeK    | N56--52       | 56.6  | Li-ion  | 26    | 024331191D |
| ASUSTeK    | X200-30       | 33.4  | Li-ion  | 26    | 9ED9651BBD |
| ASUSTeK    | X453-42       | 31.6  | Li-ion  | 26    | 4FB6C852BB |
|            | Battery       |       |         | 26    | 259A32BE33 |
| Hewlett... | Primary       | 38    | Li-ion  | 26    | CF41AB5F1A |
| Lenovo     |               | 32    |         | 26    | CC7193A7B9 |
| ASUSTeK    | K56--30       | 44.6  | Li-ion  | 25    | 469E04E0D5 |
| LGC        | L11L6Y01      | 43.6  | Li-ion  | 25    | ACACD85E54 |
| MSI        | BIF0_9        | 53.2  | Li-ion  | 25    | 1A1634FC24 |
| SMP        | bq20z451      | 63.1  | Li-ion  | 25    | 2458B122E5 |
| Samsung    |               | 45    | Li-ion  | 24    | 539459E889 |
| Sony       | PABAS024      | 52.7  | Li-ion  | 24    | 1D59A8F384 |
| ASUSTek    | PA3533U       | 51.4  | Li-ion  | 23    | 9A249DA8EE |
| LGC        | AC14B18J      | 36.7  | Li-ion  | 23    | A1D2E02773 |
| Hewlett... | Primary       | 36    | Li-ion  | 22    | 419900511F |
| Hewlett... | Primary       | 37    | Li-ion  | 22    | AB69AA73CD |
| Hewlett... | Primary       | 47    | Li-ion  | 22    | DDF30C670A |
| Hewlett... | Primary       | 89    | Li-ion  | 22    | 504BD53ED5 |
| SANYO      | AP13B3K       | 53.4  | Li-ion  | 22    | E0BC700445 |
| Hewlett... | Primary       | 49    | Li-ion  | 21    | 73E5B46F66 |
| Hewlett... | Primary       | 50    | Li-ion  | 21    | 3240076AA6 |
| SANYO      | AL10A31       | 24.4  | Li-ion  | 21    | 599BB74F20 |
| Sony       |               | 42    | Li-ion  | 21    | B5FB03D2E4 |
| Hewlett... | Primary       | 30    | Li-ion  | 20    | 76C5526BE8 |
| SANYO      | AS09A31       | 47.5  | Li-ion  | 20    | A9096F6AE6 |
| Sony       |               | 51    | Li-ion  | 20    | 1C35FEA298 |
|            | 48.84         | 49    | Li-ion  | 19    | D87F262565 |
| ASUSTeK    | K55--47       | 51.7  | Li-ion  | 19    | 55CEFCDC0B |
| ASUSTEK    | PA3533U       | 57.7  | Li-ion  | 19    | 0051F69751 |
| ASUSTek    | X401-44       | 47.5  | Li-ion  | 19    | 908F38EB2A |
| OEM        | AS10D31       | 48.4  | Li-ion  | 19    | 4A1964118D |
| Panasonic  | AS10D51       | 47.5  | Li-ion  | 19    | ACFD67A4EA |
| SANYO      | AS07B31       | 65.1  | Li-ion  | 19    | 884C33EE98 |
| SMP        | L16M2PB1      | 30    | Li-poly | 19    | 5A0A25C9FF |
| Sony       | CONIS41       | 48.8  | Li-ion  | 19    | 2589B300F1 |
|            | 48.6          | 49    | Li-ion  | 18    | 2DCF55334F |
| ASUSTeK    | X551-30       | 32.6  | Li-ion  | 18    | 5AD90522EC |
| Hewlett... | Primary       | 34    | Li-ion  | 18    | FBC2F0A547 |
| Hewlett... | Primary       | 35    | Li-ion  | 18    | 9BE61E9A2D |
| Lenovo     | BCL3100LiON   | 43.1  | Li-ion  | 18    | F9F3745636 |
| SANYO      | AS07A31       | 48.8  | Li-ion  | 18    | 3EBC919E62 |
| ASUSTeK    | X550E26       | 37.4  | Li-ion  | 17    | 7297A28A7F |
| DP         | bq20z451      | 95.0  | Li-ion  | 17    | 80ECBDE76E |
| Hewlett... | Primary       | 28    | Li-ion  | 17    | 5A84E64836 |
| Lenovo ... |               | 32    |         | 17    | 87D6FAC6F8 |
| Lenovo     | L11S6Y01      | 38.8  | Li-ion  | 17    | 5710B81F4D |
| SMP        | 45N1045       | 42.7  | Li-ion  | 17    | 368DCE09B7 |
| Toshiba    | PA3533U       | 56.1  | Li-ion  | 17    | 377FEBC80C |
| Acer       | Primary       | 44    | Li-ion  | 16    | F63B72DAC0 |
| Hewlett... | Primary       | 29    | Li-ion  | 16    | A96D39DDA2 |
| Hewlett... | Primary       | 31    | Li-ion  | 16    | BDDCB664D8 |
| Lenovo ... |               | 28    |         | 16    | 663BC4D39A |
| Lenovo     | LCFC          | 31.6  |         | 16    | 10DEE916FF |
| SIMPLO     | MWL32b        | 48.8  | Li-ion  | 16    | E07AB48C55 |
| Sony       | GC86503SY90   | 51.3  | Li-ion  | 16    | 719BE91D02 |
| ASUSTek    | M50--22       | 47.3  | Li-ion  | 15    | 69FF33D4E9 |
| ASUSTeK    | N550-40       | 60.4  | Li-ion  | 15    | 31F0B418F9 |
| ASUSTeK    | N750-62       | 69.4  | Li-ion  | 15    | 8E10CD5377 |
| Compal     | PABAS024      | 54.2  | Li-ion  | 15    | 877057DB38 |
| Hewlett... | Primary       | 32    | Li-ion  | 15    | 2D18EDEC4D |
| SIMPLO     | DELL 4DMNG31N | 66.6  | Li-ion  | 15    | 3935C68D79 |
| Simplo     | Li_Ion_4000mA | 48.8  | Li-ion  | 15    | F130F0DF5C |
| Toshiba    | PA3817U-1BRS  | 47.5  | Li-ion  | 15    | 57BAB28E56 |
|            |               | 47    | Li-ion  | 14    | 052EF081E3 |
| Compal     | PA3817U-1BRS  | 47.5  | Li-ion  | 14    | 0C917B1E8C |
| Hewlett... | 5100          | 55.0  |         | 14    | 831EAB83E8 |
| Hewlett... | Primary       | 27    | Li-ion  | 14    | E2EB1F026B |
| SANYO      | 45N1001       | 56.1  | Li-ion  | 14    | 4D1B987AAC |
| Sony       | VGP-BPS35A    | 42.3  | Li-ion  | 14    | 1CA46CE89B |
| ASUSTek    | N61--52       | 57.2  | Li-ion  | 13    | 6B772C892D |
| Hewlett... | Primary       | 22    | Li-ion  | 13    | 8F23861866 |
| PANASONIC  | AS16A5K       | 41.4  | Li-ion  | 13    | 9FBE4A663A |
| PANASONIC  | PABAS024      | 57.5  | Li-ion  | 13    | 13E3FEA8B4 |
| SANYO      | 45N1773       | 23.2  | Li-ion  | 13    | 4E682AC524 |
| SANYO      | L11S6Y01      | 41.9  | Li-ion  | 13    | ECF9D3F57B |
| SMP        | DELL GPM0365  | 97.0  | Li-ion  | 13    | 9CA695A346 |
| Sony       | AS10D41       | 47.5  | Li-ion  | 13    | B13B756085 |
| Toshiba    | PA3465U       | 65.1  | Li-ion  | 13    | 04FEFA3CE2 |
| ASUSTek    | 1001PX        | 46.4  | Li-ion  | 12    | DDBC734D1C |
| ASUSTek    | 1025C         | 56.1  | Li-ion  | 12    | F923747F5E |
| ASUSTek    | K53--26       | 39    | Li-ion  | 12    | 070E696863 |
| Hewlett... | Primary       | 16    | Li-ion  | 12    | F4B4B7905C |
| Hewlett... | Primary       | 23    | Li-ion  | 12    | D231B98589 |
| Hewlett... | Primary       | 33    | Li-ion  | 12    | 05F2682C5B |
| Hewlett... | Primary       | 54    | Li-ion  | 12    | 456A3DFFE5 |
| LGC        | 45N1127       | 23.4  | Li-ion  | 12    | 4E682AC524 |
| NEC        | PC-VP-BP77    | 86.5  | Li-ion  | 12    | 61E9D19C43 |
| Notebook   | BAT           | 77    | Li-ion  | 12    | FF0FE48329 |
| SANYO      | GARDA31       | 48.8  | Li-ion  | 12    | CD4578E8E4 |
| SANYO      | L10S6Y01      | 47.5  | Li-ion  | 12    | 17BE920F30 |
| Toshiba    | PA3534U       | 46.6  | Li-ion  | 12    | CE608A0D1E |
| Toshiba    | PA3534U-1BRS  | 46.6  | Li-ion  | 12    | E6C9D1B45A |
| 131-42-6E  | HS04041       | 42.0  | Li-ion  | 11    | 1CC2218397 |
| ASUSTek    | 1001PXD       | 23.7  | Li-ion  | 11    | CC9E23776C |
| ASUSTEK    | A8---24       | 51.2  | Li-ion  | 11    | FAECBEFA9B |
| ASUSTek    | N55--52       | 57.2  | Li-ion  | 11    | 1D32FC2BDA |
| ASUSTEK    | UL50-56       | 84    | Li-ion  | 11    | 6439D633F9 |
| ASUSTek    | X202-51       | 38.0  | Li-ion  | 11    | 5BB3C668C2 |
| ASUSTeK    | X551-26       | 37.4  | Li-ion  | 11    | EFA97B65E3 |
| CPT-COS    | L16C2PB2      | 30.6  | Li-poly | 11    | 15C78EBC5E |
| Hewlett... | Primary       | 26    | Li-ion  | 11    | 69CEE0DC90 |
| Hewlett... | Primary       | 5     | Li-ion  | 11    | 77770402E0 |
| Hewlett... | Primary       | 57    | Li-ion  | 11    | 240B48EAA4 |
| Notebook   | BAT           | 33    | Li-ion  | 11    | D4EAA0F0C1 |
| PANASONIC  | AS10B5E       | 64.8  | Li-ion  | 11    | 973B44CBCE |
| SANYO      | AC13C34       | 30.0  | Li-ion  | 11    | F08AFAA79D |
| SDI        | Dell          | 49    | Li-ion  | 11    | 44D4B713EC |
| TPS        | S10           | 36.5  | Li-ion  | 11    | 3E70A8DFF1 |
|            | 94.576        | 95    | Li-ion  | 10    | 71B1D42943 |
| ASUSTek    | 1215N         | 57.7  | Li-ion  | 10    | 1D126E3917 |
| ASUSTek    | UX32-65       | 48.2  | Li-ion  | 10    | E3825A8890 |
| ASUSTek    | X102-30       | 33.4  | Li-ion  | 10    | A7F7276E3D |
| ASUSTeK    | X402--38      | 38.0  | Li-ion  | 10    | 39FEC50B91 |
| Hewlett... | Primary       | 18    | Li-ion  | 10    | 6AF35F9DAD |
| Hewlett... | Primary       | 63    | Li-ion  | 10    | 18D94651FD |
| Hewlett... | Primary       | 73    | Li-ion  | 10    | 51BA7CEE66 |
| LGC        | 42T4911       | 56.1  | Li-ion  | 10    | D10B82AD22 |
| LION       |               | 48    |         | 10    | 4BBB490EBC |
| OEM        | AS10D51       | 48.4  | Li-ion  | 10    | 4A0A696E9A |
| PAC        | BAT           | 48    |         | 10    | 3E8051F49C |
| SMP-SDI2.8 | DELL FW1MN31  | 41.4  | Li-ion  | 10    | F85FEC55BB |
| Sony       |               | 52    | Li-ion  | 10    | 9E7981F839 |
| 111-83-72  | HS04041       | 39.5  | Li-ion  | 9     | 4FF0905A4F |
|            | 43.2          | 43    | Li-ion  | 9     | 1A72697C61 |
|            | 48.84         | 49    |         | 9     | CE35F353AC |
| ASUSTek    | K72J-44       | 48.4  | Li-ion  | 9     | E9B489DE35 |
| Compal     | Li_Ion_4000mA | 47.5  | Li-ion  | 9     | D138E04435 |
| Hewlett... | 4400          | 48.8  | Li-ion  | 9     | 45D2217223 |
| Hewlett... | Primary       | 14    | Li-ion  | 9     | 5FF106A10D |
| Hewlett... | Primary       | 24    | Li-ion  | 9     | C56EC0A2C5 |
| LG         | AS10D81       | 47.5  | Li-ion  | 9     | F56243C8E5 |
| LGC        | LNV-45N1      | 47.5  | Li-ion  | 9     | 8556FE8940 |
| LGC06      | L10L6Y01      | 48.8  | Li-ion  | 9     | 275850C827 |
| Notebook   | BAT           | 45    | Li-ion  | 9     | 072E0E1290 |
| Notebook   | BAT           | 62    | Li-ion  | 9     | 7716D0F2A1 |
| PAC        | PC-VP-WP93... | 47.5  |         | 9     | ACC8C72C7B |
| PANASONIC  | AS07B51       | 48.6  | Li-ion  | 9     | E6FF05AC01 |
| Samsung    |               | 38    | Li-ion  | 9     | 73EFFCB164 |
| SANYO      | AC14B13J      | 38.2  | Li-ion  | 9     | 5C330828A8 |
| SANYO      | AL12B32       | 37    | Li-ion  | 9     | 4D75D0FE0A |
| SANYO      | Chapala       | 71.0  | Li-ion  | 9     | 8028F0CCAE |
| SANYO      | NS2P3SZNJ4WR  | 48.7  | Li-ion  | 9     | 5781A29611 |
| SIMPLO     | AS10D73       | 48.8  | Li-ion  | 9     | 2DABFA1484 |
| SMP        | DELL VN3N047  | 41.4  | Li-ion  | 9     | A8122EF82D |
| SMP-SAN2.8 | DELL FW1MN41  | 41.4  | Li-ion  | 9     | CF2231949D |
| Sony       | AS09A41       | 48.8  | Li-ion  | 9     | 184B996E7B |
| Sony       |               | 63    | Li-ion  | 9     | 2A2BD0A648 |
| 13-54      | MO06062       | 55.5  | Li-ion  | 8     | A2443C2500 |
| ASUSTeK    | N551-52       | 56.1  | Li-ion  | 8     | F810A8999A |
| ASUSTeK    | TP50042       | 48.3  | Li-ion  | 8     | 4018847A8C |
| ASUSTek    | U31-58        | 81.2  | Li-ion  | 8     | A48BDA615E |
| ASUSTeK    | UX3-44        | 50.1  | Li-ion  | 8     | C370427F60 |
|            |               | 24    |         | 8     | 1AE3C738C0 |
|            |               |       | Li-ion  | 8     | 6479F7F12B |
| Hewlett... | Primary       | 11    | Li-ion  | 8     | 8B37127CB0 |
| Hewlett... | Primary       | 20    | Li-ion  | 8     | EA2A3A9DBB |
| Hewlett... | Primary       | 21    | Li-ion  | 8     | 54EA8BDC1D |
| Lenovo ... |               | 38    |         | 8     | 41499A9600 |
| Lenovo ... |               | 41    |         | 8     | 3A0F823175 |
| LGC        | 45N1011       | 86.5  | Li-ion  | 8     | 2064D92892 |
| LGC        | AC14A8L       | 52.4  | Li-ion  | 8     | F379C6ADAA |
| MSI        | PC-VP-BP77    | 55.0  | Li-ion  | 8     | 7B37D276D8 |
| Samsung    |               | 30    | Li-ion  | 8     | 706C37BC54 |
| SANYO      | 42T4791       | 47.5  | Li-ion  | 8     | 12347C1A1E |
| SANYO      | AL10C31       | 48.8  | Li-ion  | 8     | 37EFB54C3C |
| SMP        | LNV-L11M3P01  | 48.6  | Li-ion  | 8     | 553D2C16FB |
| 133-42-... | JC04041       | 42.4  | Li-ion  | 7     | 7E3B6FA95F |
|            |               | 47    | Li-ion  | 7     | 3CCBC58BF8 |
| Acer       | Primary       | 71    | Li-ion  | 7     | 109CDB17E1 |
| ASUSTek    | 1015B         | 56.1  | Li-ion  | 7     | 0453E7F33D |
| ASUSTek    | 1215B         | 56.1  | Li-ion  | 7     | 17B560680A |
| ASUSTEK    | A6-4224       | 69.9  | Li-ion  | 7     | C1DA7EE91F |
| ASUSTeK    |               |       | Li-ion  | 7     | 8E14B6278C |
| ASUSTeK    | X502--38      | 38.0  | Li-ion  | 7     | 2867D3E608 |
|            |               | 64    |         | 7     | 9B018C1776 |
|            |               | 16    | Li-ion  | 7     | 583681EEED |
|            |               | 28    | Li-ion  | 7     | 54908845C3 |
| Compal     | AS09A61       | 47.5  | Li-ion  | 7     | A7297EDA20 |
| DYNAPACK   | HB4593R1ECW   | 56.3  | Li-ion  | 7     | 5F7A70586E |
| Generic... | Internal      | 48.8  | Li-ion  | 7     | F2981C1775 |
| Hewlett... | Primary       | 1     | Li-ion  | 7     | 5725E675B6 |
| Hewlett... | Primary       | 56    | Li-ion  | 7     | E0DF895DC8 |
| Hewlett... | Primary       | 62    | Li-ion  | 7     | 4DADC69EFA |
| Hewlett... | Primary       | 8     | Li-ion  | 7     | 19D5DAD934 |
| Hewlett... |               | 36    | Li-ion  | 7     | F47ABA591C |
| Lenovo     | LNV-L11M6Y01  | 42.7  | Li-ion  | 7     | 7BD75B3F89 |

